#!/bin/bash

# Kafka installation directory
KAFKA_DIR="$1"

# Kill all parallel processes when this script is terminated
trap terminate SIGINT
terminate(){
    pkill -SIGINT -P $$
    exit
}

# Start zookeeper
"$KAFKA_DIR/bin/zookeeper-server-start.sh" "$KAFKA_DIR/config/zookeeper.properties" & sleep 4

# Start up 3 Kafka service brokers
"$KAFKA_DIR/bin/kafka-server-start.sh" "$KAFKA_DIR/config/server1.properties" & sleep 4
"$KAFKA_DIR/bin/kafka-server-start.sh" "$KAFKA_DIR/config/server2.properties" & sleep 4
"$KAFKA_DIR/bin/kafka-server-start.sh" "$KAFKA_DIR/config/server3.properties" &

wait