#!/bin/bash

PORT=9093
BOOTSTRAP_SERVER="localhost:$PORT"
KAFKA_DIR="$1" # Kafka installation directory

# Create the required topics
for topic in file_write file_read file_read_response heartbeat; do
  "$KAFKA_DIR/bin/kafka-topics.sh" --create --replication-factor 3 --partitions 1 --topic $topic --bootstrap-server $BOOTSTRAP_SERVER
done