public type WriteFileMessage record {
    string key;
    string content;
    int serverId?;
};

public type ReadFileMessage record {
    string key;
};

public type FileDataMessage record {
    string key;
    // JSON string
    string content;
    // ID of the server where this file came from
    int serverId;
    int[] vectorClock;
};

enum ClockComparison {
    ClockComparison_Newer,
    ClockComparison_Older,
    ClockComparison_Concurrent
}

public type ApiFileWrite record {
    string key;
    // JSON string
    string content;
};

public type ApiFileReadResponse record {
    FileDataMessage[] files;
    boolean hasConcurrentLatestVersions;
};
