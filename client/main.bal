import ballerinax/kafka;
import ballerina/log;
import ballerina/http;

// string bootstrapServer = kafka:DEFAULT_URL;
string bootstrapServer = "localhost:9093";
int httpServerPort = 9090;
// Number of servers to wait for files from
int replicationFactor = 3;

kafka:ProducerConfiguration producerConfiguration = {
    clientId: "client-producer",
    // Ask for acknowledgments from all replicas after writing
    acks: "all",
    retryCount: 3
};

function getKafkaProducer() returns kafka:Producer|error {
    return new kafka:Producer(bootstrapServer, producerConfiguration);
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns json|error {
    string messageContent = check string:fromBytes(kafkaRecord.value);
    return messageContent.fromJsonString();
}

function processRecords(kafka:ConsumerRecord[] records) returns json[]|error {
    json[] processedRecords = [];
    // The set of Kafka records received by the service are processed one by one.
    foreach int i in 0 ... records.length() - 1 {
        processedRecords[i] = check processKafkaRecord(records[i]);
    }
    return processedRecords;
}

function writeFile(WriteFileMessage message) returns error? {
    kafka:Producer producer = check getKafkaProducer();
    check producer->send({
        key: message.'key.toBytes(),
        topic: "file_write",
        value: message.toJsonString().toBytes()
    });
    check producer->'flush();
    check producer->close();
}

// Returns:
// - `Older`, if clock 1 is older than clock 2
// - `Newer`, if clock 1 is newer than clock 2
// - `Identical`, if the clocks are identical
function compareClocks(int[] vectorClock1, int[] vectorClock2) returns ClockComparison {
    int[] comparisons = [];
    boolean newer = true;
    boolean older = true;
    foreach int i in 0 ... vectorClock1.length() - 1 {
        comparisons[i] = vectorClock1[i] - vectorClock2[i];
        if (comparisons[i] > 0) {
            older = false;
        }
        if (comparisons[i] < 0) {
            newer = false;
        }
    }
    if (newer) {
        return ClockComparison_Newer;
    } else if (older) {
        return ClockComparison_Older;
    } else {
        // If identical or its not clear which came first, treat them as concurrent
        return ClockComparison_Concurrent;
    }
}

function processFileDataTopic(string fileKey, kafka:ConsumerRecord[] records) returns ApiFileReadResponse|error {
    json[] processedRecords = check processRecords(records);
    boolean noSingleLatestVersion = false;
    FileDataMessage[] files = [];
    int latestFileIndex = -1;
    FileDataMessage? latestFile = ();
    foreach int i in 0 ... processedRecords.length() - 1 {
        var processedRecord = processedRecords[i];
        FileDataMessage file = check processedRecord.cloneWithType(FileDataMessage);

        // Only process files with a matching key
        if (file.'key != fileKey) {
            continue;
        }

        files[i] = file;
        log:printInfo("Received file object: " + file.toString());

        if latestFile != () {
            ClockComparison comparison = compareClocks(file.vectorClock, latestFile.vectorClock);
            if comparison === ClockComparison_Newer {
                latestFile = file;
                latestFileIndex = i;
            // Note: This only checks if there is more than one latest version, not if all the versions are concurrent.
            } else if (comparison === ClockComparison_Concurrent && i == processedRecords.length() - 1) {
                // If we've reached the last record and its identical to the last found latest version
                // print all records out as it means there is no single latest file.
                noSingleLatestVersion = true;
                break;
            }
        } else {
            latestFile = file;
        }
    }

    // Update all old files
    if (latestFile != ()) {
        FileDataMessage[] oldFiles = [];
        foreach int i in 0 ... files.length() - 1 {
            if i != latestFileIndex {
                var file = files[i];
                ClockComparison comparison = compareClocks(file.vectorClock, latestFile.vectorClock);
                if (comparison === ClockComparison_Older) {
                    oldFiles.push(file);
                }
            }
        }

        if oldFiles.length() > 0 {
            log:printInfo("Updating " + oldFiles.length().toString() + " old files...");
            foreach var oldFile in oldFiles {
                // Update the old files contents to match the latest ones
                error? err = writeFile({
                    'key: oldFile.'key,
                    serverId: oldFile.serverId,
                    content: latestFile.content
                });
                if err is error {
                    log:printError("Failed to update file '" + oldFile.'key + "' on server '" + oldFile.serverId.toString() + "':", err);
                } else {
                    log:printInfo("Updated file '" + oldFile.'key + "' on server '" + oldFile.serverId.toString() + "' to match the latest version from server " + latestFile.serverId.toString());
                }
            }
            log:printInfo("Updated " + oldFiles.length().toString() + " old files.");
        }
    }

    ApiFileReadResponse response = {
        files: [],
        hasConcurrentLatestVersions: false
    };
    // In case there are multiple concurrent versions, print them all out. 
    if noSingleLatestVersion {
        response.files = files;
        response.hasConcurrentLatestVersions = true;
    } 
    // Otherwise, print the latest version
    else if latestFile != () {
        response.files = [latestFile];
    } else {
        // this should never happen
        return error("Error determining latest file version");
    }
    return response;
}

kafka:Consumer fileDataConsumer = check new (bootstrapServer, {
    groupId: "client-file-consumer",
    topics: ["file_read_response"],
    pollingInterval: 1,
    autoCommit: true,
    offsetReset: "latest"
});

function readFile(ReadFileMessage message) returns ApiFileReadResponse|error {
    kafka:Producer producer = check getKafkaProducer();
    check producer->send({
        key: message.'key.toBytes(),
        topic: "file_read",
        value: message.toJsonString().toBytes()
    });
    check producer->'flush();
    check producer->close();

    kafka:ConsumerRecord[] allRecords = [];
    // Poll for a response from each server. If this is not done, polling will stop after the first response.
    foreach int i in 0 ... replicationFactor - 1 {
        kafka:ConsumerRecord[] records = check fileDataConsumer->poll(1);
        records.forEach(function(kafka:ConsumerRecord r) {
            allRecords.push(r);
        });
        // If we have a response from each server already, stop.
        if allRecords.length() >= replicationFactor {
            break;
        }
    }
    log:printInfo("Received " + allRecords.length().toString() + " file(s)");
    if allRecords.length() > 0 {
        return processFileDataTopic(message.'key, allRecords);
    } else {
        return error("No files were returned by the server");
    }
}

service /files on new http:Listener(httpServerPort) {
    resource function post write(@http:Payload {} ApiFileWrite payload) returns http:InternalServerError|http:Ok {
        log:printInfo("Write file '" + payload.'key + "' to server");
        error? err = writeFile({
            'key: payload.'key, // basically the filename
            content: payload.content
        });
        if err is error {
            log:printError("[http:error | files/write]: ", err);
            return <http:InternalServerError>{
                body: {
                    'error: "Failed to write file",
                    message: err.message()
                }
            };
        } else {
            return <http:Ok>{};
        }
    }

    resource function get 'read(string fileKey) returns http:InternalServerError|http:Ok {
        log:printInfo("Attempting to read file '" + fileKey + "'");
        ApiFileReadResponse|error response = readFile({'key: fileKey});
        if response is error {
            log:printError("[http:error | files/write]: ", response);
            return <http:InternalServerError>{
                body: {
                    'error: "Failed to read file with key '" + fileKey + "'",
                    message: response.message()
                }
            };
        } else {
            return <http:Ok>{body: (response)};
        }
    }
}

public function main() returns error? {
    _ = check fileDataConsumer->poll(1);
}
