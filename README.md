# DSA Assignment 2

This project implements a distributed storage system that employs vector clocks using Ballerina and Kafka.

## Project overview

### Kafka Topics

- `file_write` - Used to send file write requests to server replicas.
- `file_read` - Used to send file read requests to server replicas.
- `file_read_response` - Sent by server replicas and read by clients in response to `file_read` requests.
- `heartbeat` - Sent and received by server replicas periodically to keep their logical vector clocks in sync.

### File structure

Once the application is run, some new folders will appear. They contain the H2 databases used to keep track of where server replica files are saved, file vector clocks and server vector clocks. They also contain the actual JSON files saved by the server replicas to the disk.

- `target`
  - `bbes`
    - `java_jdbc0.mv.db` - H2 database for server replica 0
    - `java_jdbc1.mv.db` - H2 database for server replica 1
    - `java_jdbc2.mv.db` - H2 database for server replica 2
  - `file_objects`
    - `server_0` - JSON file objects saved by server replica 0
      - `filename.json`
    - `server_1`  - JSON file objects saved by server replica 1
      - `filename.json`
    - `server_2`  - JSON file objects saved by server replica 2
      - `filename.json`

## Usage

### Dependencies

- Kafka - 2.13-3.0.0
- Ballerina - Swan Lake Beta 2

### Configure Kafka

Create 3 Kafka server property files with the following:

Server 1:

```toml
broker.id=1
listeners=PLAINTEXT://:9093
log.dirs=/tmp/kafka-logs-1
log.segment.bytes=30000000
```

Server 2:

```toml
broker.id=2
listeners=PLAINTEXT://:9094
log.dirs=/tmp/kafka-logs-2
log.segment.bytes=30000000
```

Server 3:

```toml
broker.id=3
listeners=PLAINTEXT://:9095
log.dirs=/tmp/kafka-logs-3
log.segment.bytes=30000000
```

> Note that we set the default log size to 30MB for each server.

Alternatively, use the config files found in this repo under `./config/kafka`.

### Initialize Kafka with the required topics

```bash
chmod +x ./init.sh
./init.sh
```

### Start the Kafka cluster

These should be run from wherever your Kafka instance is installed. E.g. `~/Downloads/kafka_2.13-3.0.0/`;

```bash
# Start zookeeper
bin/zookeeper-server-start.bat config/zookeeper.properties

# Start up 3 Kafka service brokers (in different terminal)
bin/kafka-server-start.sh config/server1.properties
bin/kafka-server-start.sh config/server2.properties
bin/kafka-server-start.sh config/server3.properties
```

### Start the Ballerina server replicas

```bash
# Run each in a different terminal
bal run server -- --id 0
bal run server -- --id 1
bal run server -- --id 2
```

> Note: There is also a command line argument `replication_factor` that allows changing the server replication factor, but each time its changed, the ballerina database generation code must also be changed, otherwise the vector clocks won't make sense.

### Start the Ballerina client

```bash
bal run client
```

You should then see something like:

```bash
[ballerina/http] started HTTP/WS listener 0.0.0.0:9090
```

Indicating that the program is ready to receive requests via HTTP.

### HTTP Requests

#### Write File

Writes a new file object to all server replicas.

```bash
curl --request POST \
  --url http://localhost:9090/files/write \
  --header 'Content-Type: application/json' \
  --data '{
 "key": "filename",
 "content": "{\"name\": \"David\", \"description\": \"Some example JSON content\"}"
}'
```

##### Example response

```json
{
  "files": [
    {
      "key": "filename",
      "content": "{\"name\":\"David\", \"description\":\"Some example JSON content\"}",
      "serverId": 1,
      "vectorClock": [
        4021,
        3992,
        3818
      ]
    },
    {
      "key": "filename",
      "content": "{\"name\":\"David\", \"description\":\"Some example JSON content\"}",
      "serverId": 0,
      "vectorClock": [
        4025,
        3990,
        3818
      ]
    },
    {
      "key": "filename",
      "content": "{\"name\":\"David\", \"description\":\"Some example JSON content\"}",
      "serverId": 2,
      "vectorClock": [
        4021,
        3990,
        3823
      ]
    }
  ],
  "hasConcurrentLatestVersions": true
}
```

#### Read File

Reads a file from all servers and returns the latest, or all if there are concurrent versions. It also updates any old versions stored in the server replicas. This can be observed in the Ballerina server and client logs.

```bash
curl --request GET \
  --url 'http://localhost:9090/files/read?fileKey=filename'
```

To test that this does update old files, try stopping one of the Ballerina servers, overwriting a file that already exists on all replicas, and then reading it. You should then see something like the following in your Ballerina client log:

```bash
time = 2021-10-24T17:23:43.993+02:00 level = INFO module = user/client message = "Updating 1 old files..."
time = 2021-10-24T17:23:44.017+02:00 level = INFO module = user/client message = "Updated file \'filename\' on server \'2\' to match the latest version from server 0"
time = 2021-10-24T17:23:44.020+02:00 level = INFO module = user/client message = "Updated 1 old files."
```
