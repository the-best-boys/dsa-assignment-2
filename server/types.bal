public type ClockHeartbeatMessage record {
    int serverId;
    int[] vectorClock;
};

public type WriteFileMessage record {
    string key;
    string content;
    int serverId?;
};

public type ReadFileMessage record {
    string key;
};

public type FileDataMessage record {
    string key;
    // JSON string
    string content;
    // ID of the server where this file came from
    int serverId;
    int[] vectorClock;
};

public type SQLFileData record {
    string fileKey;
    string path;
    int clockP0;
    int clockP1;
    int clockP2;
};

public type SQLServerClock record {
    int serverId;
    int value;
};
