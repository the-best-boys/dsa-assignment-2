import ballerinax/kafka;
import ballerina/task;
import ballerina/log;
import ballerinax/java.jdbc;
import ballerina/sql;
import ballerina/io;
import ballerina/uuid;

string bootstrapServer = "localhost:9093";

string dbUser = "rootUser";
string dbPassword = "rootPass";
// Prefix added to vector clock columns in the database. E.g. "clockP0".
string dbClockColPrefix = "clockP";

// ID set using command line args for this server replica.
int serverId = 0;
// WARNING: If this is changed, the database creation code must also be updated as ballerina 
// doesn't allow dynamically constructing SQL queries.
int replicationFactor = 3;
// Location where the JSON file objects written by the client will be stored.
string fileObjectPath = "./target/file_objects";
int[] vectorClock = [];
// Frequency of heartbeat messages in seconds
decimal heartbeatFrequency = 1;

boolean debugVectorClock = false;

function getKafkaProducer() returns (kafka:Producer)|error {
    kafka:ProducerConfiguration producerConfiguration = {
        clientId: "server" + serverId.toString()
    };
    return new kafka:Producer(bootstrapServer, producerConfiguration);
}

function connectToDatabase() returns jdbc:Client|error {
    return new jdbc:Client("jdbc:h2:file:./target/bbes/java_jdbc" + serverId.toString(), dbUser, dbPassword);
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns json|error {
    string messageContent = check string:fromBytes(kafkaRecord.value);
    return messageContent.fromJsonString();
}

function processRecords(kafka:Caller caller, kafka:ConsumerRecord[] records) returns json[]|error {
    json[] processedRecords = [];
    // The set of Kafka records received by the service are processed one by one.
    foreach int i in 0 ... records.length() - 1 {
        processedRecords[i] = check processKafkaRecord(records[i]);
    }

    // Commits offsets of the returned records by marking them as consumed.
    kafka:Error? commitResult = caller->commit();

    if commitResult is error {
        log:printError("Error occurred while committing the offsets for the consumer ", 'error = commitResult);
    }

    return processedRecords;
}

function writeToFile(string path, string jsonString) returns error? {
    json data = check jsonString.fromJsonString();
    int numBytes = jsonString.toBytes().length();
    // Limit files to 30,000,000 bytes = 30 MB
    if numBytes < 30000000 {
        check io:fileWriteJson(path, data);
    } else {
        return error("JSON file cannot be larger than 30 MB");
    }
}

function readFromFile(string path) returns error|json {
    return io:fileReadJson(path);
}

function dbClockColumnFromIndex(int i) returns string {
    return string `${dbClockColPrefix}${i}`;
}

// Reads a vector clock from multiple clock columns of a database row.
// E.g. => [row["clockP0"], row["clockP1"], row["clockP2"]]
function vectorClockFromDbRow(record {} row) returns int[]|error {
    int[] readVectorClock = [];
    foreach int i in 0 ... replicationFactor - 1 {
        string property = dbClockColumnFromIndex(i);
        anydata clock = row[property];
        if clock is int {
            readVectorClock[i] = clock;
        } else {
            return error("Failed to read vector clock column " + i.toString() + " from database record.");
        }
    }
    return readVectorClock;
}

function saveVectorClock() returns error? {
    jdbc:Client db = check connectToDatabase();
    sql:ParameterizedQuery[] queries = [];
    foreach int i in 0 ... replicationFactor - 1 {
        queries[i] = `MERGE INTO VectorClock (serverId, value) KEY(serverId) VALUES(${i},${vectorClock[i]})`;
    }
    _ = check db->batchExecute(queries);
    if (debugVectorClock) {
        log:printInfo("Saved vector clock: " + vectorClock.toString());
    }
}

function loadVectorClock() returns error? {
    jdbc:Client db = check connectToDatabase();
    stream<SQLServerClock, error?> resultStream = db->query(`SELECT * FROM VectorClock`);
    check resultStream.forEach(function(SQLServerClock result) {
        vectorClock[result.serverId] = result.value;
    });
    if (debugVectorClock) {
        log:printInfo("Loaded saved vector clock: " + vectorClock.toString());
    }
}

function initVectorClock() {
    error? err = loadVectorClock();
    // If there was no saved vector clock, start with a blank one.
    if err is error {
        log:printError("Failed to load saved vector clock: ", err);
        foreach int i in 0 ... replicationFactor - 1 {
            vectorClock[i] = 0;
        }
    }
    log:printInfo("Initialized vector clock to: " + vectorClock.toString());
}

function incrementClock() {
    vectorClock[serverId] += 1;
}

function syncToClock(int[] vectorClock2) {
    foreach int i in 0 ... replicationFactor - 1 {
        vectorClock[i] = vectorClock[i].max(vectorClock2[i]);
    }
    if debugVectorClock {
        log:printInfo("Synced clock to " + vectorClock2.toString());
    }
}

class HeartbeatJob {
    *task:Job;
    kafka:Producer producer;

    public function execute() {
        // We are sending an event so increment the clock for this process.
        incrementClock();
        error? err = saveVectorClock();
        if (debugVectorClock) {
            if (err is error) {
                log:printError("Failed to save vector clock: ", err);
            }
            log:printInfo("[Out] Vector clock = " + vectorClock.toString());
        }

        ClockHeartbeatMessage msg = {
            serverId: serverId,
            vectorClock: vectorClock
        };
        err = self.producer->send({
            topic: "heartbeat",
            value: msg.toJsonString().toBytes()
        });
        if (err is error) {
            log:printError("Failed to send heartbeat: ", err);
        }
    }

    isolated function init(kafka:Producer producer) {
        self.producer = producer;
    }
}

function processHeartbeat(ClockHeartbeatMessage heartbeat) returns error? {
    if (heartbeat.serverId != serverId) {
        incrementClock();
        syncToClock(heartbeat.vectorClock);
        error? err = saveVectorClock();

        // TODO: Handle vector clock resetting.
        // Each clock can only go up to 64-bits and will need to reset at that point.

        if (debugVectorClock) {
            if (err is error) {
                log:printError("Failed to save vector clock: ", err);
            }
            log:printInfo("[In] Vector clock = " + vectorClock.toString());
        }
    }
}

function processHeartbeatTopic(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
    json[] processedRecords = check processRecords(caller, records);
    foreach var processedRecord in processedRecords {
        ClockHeartbeatMessage heartbeat = check processedRecord.cloneWithType(ClockHeartbeatMessage);
        check processHeartbeat(heartbeat);
    }
}

listener kafka:Listener kafkaHeartbeatListener = new (bootstrapServer, {
    // Use a UUID so multiple servers can consume the same messages
    groupId: uuid:createType4AsString(),
    topics: ["heartbeat"],
    pollingInterval: heartbeatFrequency / 2,
    autoCommit: false
});

service kafka:Service on kafkaHeartbeatListener {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        error? ret = processHeartbeatTopic(caller, records);
        if ret is error {
            log:printError("[topic:heartbeat]: ", ret);
        }
    }
}

function writeFileObject(WriteFileMessage fileData) returns error? {
    int? targetServerId = fileData?.serverId;
    if (targetServerId == () || targetServerId == serverId) {
        jdbc:Client db = check connectToDatabase();

        // Write the data to an actual file
        string filePath = fileObjectPath + "/server" + serverId.toString() + "/" + fileData.'key + ".json";
        check writeToFile(filePath, fileData.content);
        log:printInfo(string `Wrote ${fileData.'key} to file ${filePath}`);

        // TODO: Dynamically insert clocks based on replication factor. 
        // The following doesn't work because balerina doesn't allow dynamic SQL queries.
        // 
        // string vectorClockColumnNames = "";
        // string vectorClockColumnValues = "";
        // foreach int i in 0 ... replicationFactor - 1 {
        //     vectorClockColumnNames += dbClockColumnFromIndex(i) + ",";
        //     vectorClockColumnValues += vectorClock[i].toString() + ",";
        // }

        // Insert or update if the key already exists
        _ = check db->execute(`
            MERGE INTO Files (fileKey, clockP0, clockP1, clockP2, path)
            KEY(fileKey)
            VALUES (${fileData.'key}, ${vectorClock[0]}, ${vectorClock[1]}, ${vectorClock[2]}, ${filePath}) 
        `);
        log:printInfo(string `Added file key '${fileData.'key}' to database`);
    }
}

function readFileObject(ReadFileMessage request) returns error? {
    log:printInfo("Received request for file: " + request.'key);
    // Find the file in the database with the same key
    jdbc:Client db = check connectToDatabase();
    stream<SQLFileData, error?> resultStream = db->query(`SELECT * FROM Files WHERE fileKey=${request.'key} LIMIT 1`);
    record {|SQLFileData value;|}|error? result = resultStream.next();
    if result is record {|SQLFileData value;|} {
        SQLFileData fileData = result.value;
        log:printInfo("Found file " + request.'key + " at " + fileData.path);

        int[] fileVectorClock = check vectorClockFromDbRow(result.value);

        // Read the file from storage
        json|error fileContent = readFromFile(fileData.path);
        if fileContent is json {
            // Send it back to the client
            FileDataMessage msg = {
                'key: fileData.fileKey,
                serverId: serverId,
                content: fileContent.toJsonString(),
                vectorClock: fileVectorClock
            };
            kafka:Producer kafkaProducer = check getKafkaProducer();
            // TODO: Send back errors as well
            check kafkaProducer->send({
                'key: fileData.fileKey.toBytes(),
                topic: "file_read_response",
                value: msg.toJsonString().toBytes()
            });
            check kafkaProducer->close();
            log:printInfo("Sent back file data for " + request.'key);
        } else {
            return error("Failed to read JSON file from path " + fileData.path);
        }
    } else {
        // TODO: Consider sending error back to client
        return error("Failed to find file with key " + request.'key);
    }
}

function processWriteFileTopic(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
    json[] processedRecords = check processRecords(caller, records);
    foreach var processedRecord in processedRecords {
        WriteFileMessage fileData = check processedRecord.cloneWithType(WriteFileMessage);
        check writeFileObject(fileData);
    }
}

function processReadFileTopic(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
    json[] processedRecords = check processRecords(caller, records);
    foreach var processedRecord in processedRecords {
        ReadFileMessage request = check processedRecord.cloneWithType(ReadFileMessage);
        check readFileObject(request);
    }
}

listener kafka:Listener kafkaFileWriteListener = new (bootstrapServer, {
    // Use a UUID so multiple servers can consume the same messages
    groupId: uuid:createType4AsString(),
    topics: ["file_write"],
    pollingInterval: 1,
    autoCommit: false
});

service kafka:Service on kafkaFileWriteListener {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        error? ret = processWriteFileTopic(caller, records);
        if ret is error {
            log:printError("[topic:file_write]: ", ret);
        }
    }
}

listener kafka:Listener kafkaFileRequestListener = new (bootstrapServer, {
    // Use a UUID so multiple servers can consume the same messages
    groupId: uuid:createType4AsString(),
    topics: ["file_read"],
    pollingInterval: 1,
    autoCommit: false
});

service kafka:Service on kafkaFileRequestListener {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        error? ret = processReadFileTopic(caller, records);
        if ret is error {
            log:printError("[topic:file_read]: ", ret);
        }
    }
}

public type CommandLineArgs record {
    int id;
    int replication_factor?;
};

// Initialize 
function initDatabase(jdbc:Client db) returns sql:Error? {
    // TODO: Dynamically create clocks based on replication factor. This didn't work because
    // ballerina doesn't seem to let you dynamically create RawTemplate/ParameterizedQuery strings.
    //
    // // Automatically create the right number of clock columns depending on the replication factor
    // // in order to store a vector clock.
    // string clockColumns = "";
    // foreach int i in 0 ... replicationFactor - 1 {
    //     clockColumns += string `${dbClockColumnFromIndex(i)} BIGINT,`;
    // }

    _ = check db->execute(`
        CREATE TABLE IF NOT EXISTS Files(
            fileKey VARCHAR(300) NOT NULL,
            path VARCHAR(300),
            clockP0 BIGINT,
            clockP1 BIGINT,
            clockP2 BIGINT,
            PRIMARY KEY (fileKey)
        )
    `);

    _ = check db->execute(`
        CREATE TABLE IF NOT EXISTS VectorClock(
            serverId INT NOT NULL,
            value BIGINT,
            PRIMARY KEY (serverId)
        )
    `);
}

public function main(*CommandLineArgs args) returns error? {
    // Optionally, set replication factor from command line args
    int? argReplicationFactor = args?.replication_factor;
    if argReplicationFactor is int {
        replicationFactor = argReplicationFactor;
    }

    // Set this server's ID from the command line args
    serverId = args.id;
    if (serverId >= replicationFactor) {
        return error("Max server replication factor " + replicationFactor.toString() + " reached. No more servers can be started.");
    }

    initVectorClock();

    jdbc:Client db = check connectToDatabase();
    check initDatabase(db);

    log:printInfo("Started ballerina server with ID: " + serverId.toString());

    // Send heartbeats periodically
    _ = check task:scheduleJobRecurByFrequency(new HeartbeatJob(check getKafkaProducer()), heartbeatFrequency);
    log:printInfo("Started heartbeat job");
}
