echo "Testing writing a file"
curl --request POST \
  --url http://localhost:9090/files/write \
  --header 'Content-Type: application/json' \
  --data '{
 "key": "filename",
 "content": "{\"name\": \"David\", \"description\": \"Some example JSON content\"}"
}'

echo "Waiting 7 seconds for the servers to write the file..."
sleep 7

echo "Testing reading the file"
rl --request GET \
  --url 'http://localhost:9090/files/read?fileKey=filename'